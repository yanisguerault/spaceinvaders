﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace SpaceInvaders
{
    class Vecteur2D
    {
        public double x;
        public double y;
        double norme;

        public Vecteur2D(double x = 0, double y = 0)
        {
            this.x = x;
            this.y = y;
        }

        public double Norme { get => Math.Sqrt(Math.Pow(x,2)+Math.Pow(y,2)); }

        public static Vecteur2D operator +(Vecteur2D v1, Vecteur2D v2)
        { return new Vecteur2D(v1.x + v2.x, v1.y + v2.y); }

        public static Vecteur2D operator -(Vecteur2D v1, Vecteur2D v2)
        { return new Vecteur2D(v1.x - v2.x, v1.y - v2.y); }

        public static Vecteur2D operator -(Vecteur2D v1)
        { return new Vecteur2D(-v1.x, -v1.y); }

        public static Vecteur2D operator *(Vecteur2D v1, double v2)
        { return new Vecteur2D(v1.x * v2, v1.y * v2); }

        public static Vecteur2D operator *(double v2, Vecteur2D v1)
        { return new Vecteur2D(v1.x * v2, v1.y * v2); }

        public static Vecteur2D operator /(Vecteur2D v1, double v2)
        { return new Vecteur2D(v1.x / v2, v1.y / v2); }
    }
}
