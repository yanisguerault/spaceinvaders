﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;

namespace SpaceInvaders
{
    class BlocEnnemi : GameObject
    {
        private List<Ennemi> tabEnnemi= new List<Ennemi>();
        private Bitmap[] images = { Properties.Resources.ship2, Properties.Resources.ship3, Properties.Resources.ship4, Properties.Resources.ship5, Properties.Resources.ship6, Properties.Resources.ship7, Properties.Resources.ship8, Properties.Resources.ship9 };
        private int vitesseEnnemi = 25;
        private double vitesseTir = 0.05;
        /// Si true, le bloc se déplacer vers la droite, sinon le bloc se déplace vers la gauche
        private bool right = true;
        private int nbLigne = 0;
        private static Random r = new Random();
        /// <summary>
        /// Variable permettant de savoir si il faut faire descendre le bloc (quand celui-ci arrive au bord de l'écran)
        /// </summary>
        bool descendre = false;

        public BlocEnnemi(Game gameinstance) : base()
        {
            this.SideObjet = Side.Neutre;
        }

        /// <summary>
        /// Permet d'ajouter une ligne de vaisseaux ennemis
        /// </summary>
        /// <param name="gameinstance"></param>
        /// <param name="nbennemi">Nombre d'ennemi de la ligne</param>
        /// <param name="vies">Nombre de vies des ennemis de la ligne</param>
        public void AddLineEnnemi(Game gameinstance,int nbennemi,int vies)
        {
            int random = r.Next(0, 7);
            for (int i = 0; i < nbennemi; i++)
            {
                Ennemi en = new Ennemi(new Vecteur2D(i * 50, nbLigne * 30), images[random],vies);
                tabEnnemi.Add(en);
                gameinstance.AddNewGameObject(en);
            }
            this.nbLigne++;
        }
        public override void Update(Game gameInstance, double deltaT)
        {
            moveEnnemi(gameInstance,deltaT);

                ///Permet de faire le tir aléatoire des ennemis 
            foreach(Ennemi ennemi in tabEnnemi)
            {
               if (r.NextDouble() <= vitesseTir * deltaT)
                    {
                        ennemi.TirerMissile(gameInstance);
                    }
                }

        }
        /// <summary>
        /// Fonction permettant de faire bouger le bloc ennemi
        /// </summary>
        /// <param name="gameInstance"></param>
        /// <param name="deltaT"></param>
        private void moveEnnemi(Game gameInstance, double deltaT)
        {
            ///Si la variable descendre est vrai, le bloc ennemi va descendre de 20 pixels
            if(descendre)
            {
                    foreach (Ennemi en in tabEnnemi)
                    {
                        if (en != null)
                        {
                            en.position.y += 20;
                        }
                    }

                vitesseEnnemi += 5;
                vitesseTir += 0.01;
                descendre = false;
            }
            try
            {
                ///Pour chaque ennemi, bouge la position en fonction de si on se déplace de droite a gauche ou de gauche à droite
                ///et on verifie si il faut faire descendre le bloc au prochain appel de la fonction
                ///On verifie également si le bloc est trop bas et que le jeu est perdu et on supprime de la liste les ennemis morts
                foreach (Ennemi en in tabEnnemi)
                {
                    if (en.position.x >= gameInstance.gameSize.Width - 50)
                    {
                        right = false;
                        descendre = true;
                    }
                    else if (en.position.x < 0)
                    {
                        right = true;
                        descendre = true;
                    }

                    if (right)
                        en.position.x += deltaT * vitesseEnnemi;
                    else
                        en.position.x -= deltaT * vitesseEnnemi;

                    if (en.position.y >= gameInstance.gameSize.Height - 150)
                        gameInstance.etatencours = Game.EtatJeu.Over;

                    if (!en.IsAlive())
                        tabEnnemi.Remove(en);
                }
            }
            catch { }
            //Console.WriteLine(count);
        }
        /// <summary>
        /// Pas de collision entre le bloc et les missiles, celle-ci ne comporte donc rien
        /// </summary>
        /// <param name="missile"></param>
        public override void Collision(Missile missile)
        {}

        /// <summary>
        /// Pas d'image pour le bloc, donc pas besoin de la fonction Draw
        /// </summary>
        /// <param name="gameInstance"></param>
        /// <param name="graphics"></param>
        public override void Draw(Game gameInstance, Graphics graphics)
        {}

        /// <summary>
        /// Retourne si le bloc est en vie
        /// </summary>
        /// <returns>En vie si il reste des ennemis dans le bloc</returns>
        public override bool IsAlive()
        {
            return tabEnnemi.Count > 0;
        }
    }
}
