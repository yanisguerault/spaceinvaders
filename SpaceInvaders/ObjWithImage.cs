﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace SpaceInvaders
{
    /// <summary>
    /// Classe abstraite permettant de généraliser certains paramètres et méthodes communes
    /// à tous les objets placer dans le jeu.
    /// </summary>
    abstract class ObjWithImage : GameObject
    {
        internal Vecteur2D position;

        internal bool alive = true;

        internal Bitmap image;

        public int nbVies;

        /// <summary>
        /// Constructeur plaçant l'objet 
        /// </summary>
        /// <param name="v">Vecteur2D contenant la position de l'objet</param>
        public ObjWithImage(Vecteur2D v,Bitmap image) : base()
        {
            this.position = v;
            this.image = new Bitmap(image);
        }

        public override void Draw(Game gameInstance, Graphics graphics)
        {
            for (int x = 0; x < this.image.Width; x++)
            {
                for (int y = 0; y < this.image.Height; y++)
                {
                    if (this.image.GetPixel(x, y) == Color.FromArgb(255,0,0,0))
                    {
                        this.image.SetPixel(x, y, Color.FromArgb(255, 255,255, 255));
                    }
                }
            }
            graphics.DrawImage(this.image, (float)this.position.x, (float)this.position.y, image.Width, image.Height);
        }

        public override void Collision(Missile missile)
        {
            if (!(missile.Position.x > this.position.x + image.Size.Width
                      || missile.Position.y > this.position.y + image.Size.Height
                      || this.position.x > missile.Position.x + missile.Image.Size.Width
                      || this.position.y > missile.Position.y + missile.Image.Size.Height)
                  && this.SideObjet != missile.SideObjet)
            {
                for (int x = 0; x < missile.Image.Width; x++)
                {
                    for (int y = 0; y < missile.Image.Height; y++)
                    {
                        if (missile.Position.x + x > this.position.x
                                && missile.Position.x + x < this.position.x + this.image.Width
                                && missile.Position.y + y > this.position.y
                                && missile.Position.y + y < this.position.y + this.image.Height
                                && this.image.GetPixel((int)((missile.Position.x + x - this.position.x)), (int)((missile.Position.y + y - this.position.y))) == Color.FromArgb(255, 255, 255, 255))
                        {
                            this.image.SetPixel((int)(x + (missile.Position.x - this.position.x)), (int)(y + (missile.Position.y - this.position.y)), Color.FromArgb(0, 255, 255, 255));
                            int mini = Math.Min(missile.nbVies, this.nbVies);
                            if (mini > 0)
                            {
                                this.nbVies -= 1;
                                missile.nbVies -= 1;
                            }
                        }
                    }
                }

            }
        }

        public override bool IsAlive()
        {
            return alive;
        }
    }
}