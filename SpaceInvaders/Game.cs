﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Drawing;
using System.Diagnostics;
using System.Windows.Forms;

namespace SpaceInvaders
{
    class Game
    {

        #region GameObjects management
        /// <summary>
        /// Set of all game objects currently in the game
        /// </summary>
        public HashSet<GameObject> gameObjects = new HashSet<GameObject>();

        /// <summary>
        /// Set of new game objects scheduled for addition to the game
        /// </summary>
        private HashSet<GameObject> pendingNewGameObjects = new HashSet<GameObject>();

        /// <summary>
        /// Schedule a new object for addition in the game.
        /// The new object will be added at the beginning of the next update loop
        /// </summary>
        /// <param name="gameObject">object to add</param>
        public void AddNewGameObject(GameObject gameObject)
        {
            pendingNewGameObjects.Add(gameObject);
        }
        #endregion

        #region game technical elements
        /// <summary>
        /// Size of the game area
        /// </summary>
        public Size gameSize;

        /// <summary>
        /// State of the keyboard
        /// </summary>
        public HashSet<Keys> keyPressed = new HashSet<Keys>();

        public int FramesCounters = 1;

        public enum EtatJeu {Play,Pause,Over,Win};

        public enum Level { Un,Deux,Trois,Final,None};
        public Level levelencours = Level.Un;

        public EtatJeu etatencours = EtatJeu.Play;

        #endregion

        #region static fields (helpers)

        /// <summary>
        /// Singleton for easy access
        /// </summary>
        public static Game game { get; private set; }

        /// <summary>
        /// A shared black brush
        /// </summary>
        private static Brush blackBrush = new SolidBrush(Color.Black);

        private static Brush whiteBrush = new SolidBrush(Color.White);

        /// <summary>
        /// A shared simple font
        /// </summary>
        private static Font defaultFont = new Font("Times New Roman", 24, FontStyle.Bold, GraphicsUnit.Pixel);
        #endregion


        #region constructors
        /// <summary>
        /// Singleton constructor
        /// </summary>
        /// <param name="gameSize">Size of the game area</param>
        /// 
        /// <returns></returns>
        public static Game CreateGame(Size gameSize)
        {
            if (game == null)
                game = new Game(gameSize);
            return game;
        }

        /// <summary>
        /// Private constructor
        /// </summary>
        /// <param name="gameSize">Size of the game area</param>
        private Game(Size gameSize)
        {
            this.gameSize = gameSize;
            /// Met en places les objets nécessaires pour le niveau 1
            initGamesObjects(3, new int[] { 5, 4, 3 }, 12);
        }

        #endregion

        #region methods

        /// <summary>
        /// Force a given key to be ignored in following updates until the user
        /// explicitily retype it or the system autofires it again.
        /// </summary>
        /// <param name="key">key to ignore</param>
        public void ReleaseKey(Keys key)
        {
            keyPressed.Remove(key);
        }


        /// <summary>
        /// Draw the whole game
        /// </summary>
        /// <param name="g">Graphics to draw in</param>
        public void Draw(Graphics g)
        {
            g.DrawImage(SpaceInvaders.Properties.Resources.fond, 0, 0, gameSize.Width, gameSize.Height);
            foreach (GameObject gameObject in gameObjects)
                gameObject.Draw(this, g);

            float padx = ((float)gameSize.Height) / 2 - 50;
            float pady = ((float)gameSize.Width) / 2;

            /// Met un texte en cas d'état de pause, de défaite ou de victoire
            if (etatencours == EtatJeu.Pause)
                g.DrawString("Jeu en Pause", defaultFont,whiteBrush,padx-50,pady);
            else if(etatencours == EtatJeu.Over)
                g.DrawString("Game Over", defaultFont, whiteBrush, padx-50, pady);
            else if(etatencours == EtatJeu.Win)
                g.DrawString("Good Game, you Win", defaultFont, whiteBrush, padx-100, pady);

            /// Met le numéro de niveau en bas à droite de l'écran
            if(levelencours == Level.Un)
                g.DrawString("Niveau 1", defaultFont, whiteBrush, ((float)gameSize.Height)-100, ((float)gameSize.Width)-50);
            else if(levelencours == Level.Deux)
                g.DrawString("Niveau 2", defaultFont, whiteBrush, ((float)gameSize.Height)-100, ((float)gameSize.Width) - 50);
            else if (levelencours == Level.Trois)
                g.DrawString("Niveau 3", defaultFont, whiteBrush, ((float)gameSize.Height)-100, ((float)gameSize.Width) - 50);
            else if (levelencours == Level.Final)
                g.DrawString("Niveau FINAL", defaultFont, whiteBrush, ((float)gameSize.Height) - 160, ((float)gameSize.Width) - 50);
        }

        /// <summary>
        /// Update game
        /// </summary>
        public void Update(double deltaT)
        {
            // add new game objects
            gameObjects.UnionWith(pendingNewGameObjects);
            pendingNewGameObjects.Clear();


            // if space is pressed
            /*if (keyPressed.Contains(Keys.Space))
            {
                ReleaseKey(Keys.Space);
            }*/

            /// Si état pause, on ne fait rien et on attend que l'utilisateur appuie sur P
            if (keyPressed.Contains(Keys.P))
            {
                if (etatencours == EtatJeu.Play)
                    etatencours = EtatJeu.Pause;
                else if (etatencours == EtatJeu.Pause)
                    etatencours = EtatJeu.Play;
                ReleaseKey(Keys.P);
            }

            /// Update du jeu uniquement quand le jeu est en etat Play
            if (etatencours == EtatJeu.Play)
            {
                // update each game object
                foreach (GameObject gameObject in gameObjects)
                {
                    gameObject.Update(this, deltaT);
                }

                // remove dead objects
                gameObjects.RemoveWhere(gameObject => !gameObject.IsAlive());
            }

            int countblock = 0;
            int countjoueur = 0;
            foreach(GameObject gameObject in gameObjects)
            {
                if (typeof(BlocEnnemi) == gameObject.GetType())
                {
                    countblock++;
                }
                if(typeof(Joueur) == gameObject.GetType())
                {
                    countjoueur++;
                }
            }

            /// En fonction du level en cours, si le block ennemi est détruit, passe au niveau supérieur
            switch(levelencours)
            {
                case Level.Un:
                    if (countblock < 1)
                    {
                        levelencours = Level.Deux;
                        initGamesObjects(4, new int[] { 4, 4, 5, 6 }, 12);
                    }
                    break;

                case Level.Deux:
                    if (countblock < 1)
                    {
                        levelencours = Level.Trois;
                        initGamesObjects(6, new int[] { 4, 4, 5, 6,3,3 }, 12);
                    }
                    break;

                case Level.Trois:
                    if (countblock < 1)
                    {
                        levelencours = Level.Final;
                        initGamesObjects(8, new int[] { 4, 4, 5, 6, 7, 7,2,1 }, 12);
                    }
                    break;

                case Level.Final:
                    if (countblock < 1)
                    {
                        levelencours = Level.None;
                        etatencours = EtatJeu.Win;
                    }
                    break;
            }

            /// Si plus de joueur, game over
            if (countjoueur < 1)
            {
                etatencours = EtatJeu.Over;
            }

            /// Si gagner ou game over, appuyer sur espace pour relancer au niveau 1
            if((etatencours == EtatJeu.Win || etatencours == EtatJeu.Over) && keyPressed.Contains(Keys.Space))
            {
                initGamesObjects(3,new int[] { 5, 4, 3 },12);
                levelencours = Level.Un;
                etatencours = EtatJeu.Play;
            }
        }

        /// <summary>
        ///  Fonction permettant d'init les objets nécessaires au jeu
        /// </summary>
        /// <param name="nbligne">Nombre de ligne d'ennemi</param>
        /// <param name="nbennemi">Tableau contenant le nombre d'ennemi pour chaque ligne (tab.count = nbligne)</param>
        /// <param name="nbvie">le nombre de vies des ennemis</param>
        private void initGamesObjects(int nbligne, int[] nbennemi, int nbvie)
        {
            gameObjects.Clear();
            GameObject joueur = new Joueur(new Vecteur2D(gameSize.Width / 2, gameSize.Height - 50), SpaceInvaders.Properties.Resources.ship1, 30);
            AddNewGameObject(joueur);

            BlocEnnemi bloc = new BlocEnnemi(this);
            AddNewGameObject(bloc);
            for(int i = 0; i < nbligne; i++)
            {
                bloc.AddLineEnnemi(this, nbennemi[i], nbvie);
            }

            for (int i = 0; i < 3; i++)
            {
                GameObject bunker = new Bunker(new Vecteur2D(50 + 200 * i, gameSize.Height - 100), SpaceInvaders.Properties.Resources.bunker);
                AddNewGameObject(bunker);
            }
        }
        #endregion
    }
}
