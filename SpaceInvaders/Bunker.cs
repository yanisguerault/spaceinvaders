﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Drawing;
using System.Windows.Forms;

namespace SpaceInvaders
{
    class Bunker : ObjWithImage
    {

        /// <summary>
        /// Constructeur du Bunker
        /// </summary>
        public Bunker(Vecteur2D v, Bitmap image) : base(v, image)
        {
            this.position = v;
            this.image = image;
            this.SideObjet = Side.Neutre;
        }

        /// <summary>
        /// Le bunker ne bouge pas, donc pas besoin d'update
        /// </summary>
        /// <param name="gameInstance"></param>
        /// <param name="deltaT"></param>
        public override void Update(Game gameInstance, double deltaT)
        {

        }

        /// <summary>
        /// Verifie la collision par rectangle joint puis pixel par pixel, si c'est le cas, les pixels du bunker sont effacés tant que le missile à de la vie.
        /// </summary>
        /// <param name="missile"></param>
        public override void Collision(Missile missile)
        {
            if (!(missile.Position.x > this.position.x + image.Size.Width 
                    || missile.Position.y > this.position.y + image.Size.Height 
                    || this.position.x > missile.Position.x + missile.Image.Size.Width 
                    || this.position.y > missile.Position.y + missile.Image.Size.Height) 
                && this.SideObjet != missile.SideObjet)
            {
                for(int x=0;  x < missile.Image.Width;x++)
                {
                    for(int y=0;  y <  missile.Image.Height;y++)
                    {

                    if(missile.Position.x + x > this.position.x 
                            && missile.Position.x + x < this.position.x + this.image.Width 
                            && missile.Position.y + y > this.position.y 
                            && missile.Position.y +y < this.position.y + this.image.Height
                            && this.image.GetPixel((int)((missile.Position.x + x - this.position.x)), (int)((missile.Position.y + y - this.position.y))) == Color.FromArgb(255, 255, 255, 255))
                        {
                                this.image.SetPixel((int)(x + (missile.Position.x - this.position.x)), (int)(y + (missile.Position.y - this.position.y)), Color.FromArgb(0, 255, 255, 255));
                                missile.nbVies -= 1;
                        }
                    }
                }
               
            }
        }

        /// <summary>
        /// Fonction obligatoire car héritée.
        /// </summary>
        /// <returns></returns>
        public override bool IsAlive()
        {
            return alive;
        }
    }
}
