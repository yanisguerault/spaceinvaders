﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Drawing;
using System.Windows.Forms;

namespace SpaceInvaders
{
    class Joueur : ObjWithImage
    {
        private double VitesseJoueur = 200;

        private Missile missile;

        /// <summary>
        /// Constructeur du Joueur
        /// </summary>
        public Joueur(Vecteur2D v,Bitmap image,int nbvie=1) : base(v,image)
        {
            this.position = v;
            this.image = new Bitmap(image);
            this.nbVies = nbvie;
            this.SideObjet = Side.Allie;
        }

        /// <summary>
        /// Permet de gerer les déplacements et les tirs du joueur
        /// </summary>
        /// <param name="gameInstance"></param>
        /// <param name="deltaT"></param>
        public override void Update(Game gameInstance, double deltaT)
        {
            if (gameInstance.keyPressed.Contains(Keys.Right) && position.x < gameInstance.gameSize.Width - image.Width)
                position.x += deltaT*VitesseJoueur;
            if (gameInstance.keyPressed.Contains(Keys.Left) && position.x > 0)
                position.x -= deltaT*VitesseJoueur;
            if (gameInstance.keyPressed.Contains(Keys.Space))
                TirerMissile(gameInstance);
        }

        /// <summary>
        /// Permet de dessiner le joueur ainsi que sa barre de vie sur le coté
        /// </summary>
        /// <param name="gameInstance"></param>
        /// <param name="graphics"></param>
        public override void Draw(Game gameInstance, Graphics graphics)
        {
            base.Draw(gameInstance, graphics);
            graphics.DrawString("Vie : "+ nbVies/6, new Font(FontFamily.GenericSansSerif, 12, FontStyle.Regular),
            new SolidBrush(Color.White), 20, gameInstance.gameSize.Height-50);
        }

        /// <summary>
        /// Permet de tirer des missiles (1 seule possible, possibilité de re-tirer uniquement quand le premier missile a eu une collision ou est sortie de l'écran)
        /// </summary>
        /// <param name="gameInstance"></param>
        public void TirerMissile(Game gameInstance)
        {
            if (missile == null || !missile.IsAlive())
            {
                this.missile = new Missile(new Vecteur2D(position.x + image.Width / 2, position.y), SpaceInvaders.Properties.Resources.shoot1, -1, this.SideObjet, 6);
                gameInstance.AddNewGameObject(missile);
            }
        }

        /// <summary>
        /// Permet de savoir si le vaisseau est en vie
        /// </summary>
        /// <returns>Retourne true si le nombre de vie est strictement supérieur à 0 </returns>
        public override bool IsAlive()
        {
            return nbVies > 0;
        }
    }
}
