﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Drawing;
using System.Linq;
using System.Text;

namespace SpaceInvaders
{
    class Missile : ObjWithImage
    {
        /// <summary>
        /// Vitesse du Missile
        /// </summary>
        private double vitesseMissile = 300;


        /// <summary>
        /// Lorsque la variable est a -1, le tir s'effectue vers le haut (tir pour le joueur), sinon le tir s'effectue vers le bas (tir ennemi) (=1)
        /// </summary>
        private int directionTir = 0;

        public Bitmap Image { get => image; }
        internal Vecteur2D Position { get => position; }
        public bool Alive { get => alive; set => alive = value; }

        /// <summary>
        /// Constructeur du Missile, prend en compte la direction du tir, le side du tireur et le nombre de vies du missile
        /// </summary>
        public Missile(Vecteur2D v, Bitmap image,int directiontir, Side campMissile, int nbvies) : base(v, image)
        {
            this.position = v;
            this.image = image;
            this.directionTir = directiontir;
            this.SideObjet = campMissile;
            this.nbVies = nbvies;
        }

        /// <summary>
        /// Si il dépasse l'écran
        /// </summary>
        /// <param name="gameInstance"></param>
        /// <param name="deltaT"></param>
        public override void Update(Game gameInstance, double deltaT)
        {
            if (position.y <= 0 || position.y >= gameInstance.gameSize.Width)
            {
                this.nbVies = 0;
            }
       

            position.y += deltaT*vitesseMissile* directionTir;

            foreach(GameObject objet in gameInstance.gameObjects)
            {
                objet.Collision(this);
            }
        }

        public override void Collision(Missile missile)
        {
            if (!(missile.Position.x > this.position.x + image.Size.Width
                      || missile.Position.y > this.position.y + image.Size.Height
                      || this.position.x > missile.Position.x + missile.Image.Size.Width
                      || this.position.y > missile.Position.y + missile.Image.Size.Height)
                  && this.SideObjet != missile.SideObjet)
            {
                for (int x = 0; x < missile.Image.Width; x++)
                {
                    for (int y = 0; y < missile.Image.Height; y++)
                    {
                        if (missile.Position.x + x > this.position.x
                                && missile.Position.x + x < this.position.x + this.image.Width
                                && missile.Position.y + y > this.position.y
                                && missile.Position.y + y < this.position.y + this.image.Height
                                && this.image.GetPixel((int)((missile.Position.x + x - this.position.x)), (int)((missile.Position.y + y - this.position.y))) == Color.FromArgb(255, 0, 0, 0))
                        {
                            missile.nbVies = 0;
                            this.nbVies = 0;
                        }
                    }
                }

            }
        }

        public override bool IsAlive()
        {
            return nbVies > 0;
        }
    }
}
