﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace SpaceInvaders
{
    class Ennemi : ObjWithImage
    {
        /// <summary>
        /// Constructeur des Ennemis
        /// </summary>
        public Ennemi(Vecteur2D v, Bitmap image,int nbvie=1) : base(v, image)
        {
            this.image = new Bitmap(image);
            this.position = v;
            this.nbVies = nbvie;
            this.SideObjet = Side.Ennemi;
        }

        /// <summary>
        /// C'est la classe BlocEnnemi qui s'occupe de faire bouger les ennemis.
        /// </summary>
        /// <param name="gameInstance"></param>
        /// <param name="deltaT"></param>
        public override void Update(Game gameInstance, double deltaT)
        {}

        /// <summary>
        /// Permet de faire tirer un missile a un ennemi
        /// </summary>
        /// <param name="gameInstance"></param>
        public void TirerMissile(Game gameInstance)
        {
            gameInstance.AddNewGameObject(new Missile(new Vecteur2D(position.x + image.Width / 2, position.y), Properties.Resources.shoot1,1,this.SideObjet, 6));
        }

        /// <summary>
        /// Retourne si l'ennemi est en vie
        /// </summary>
        /// <returns>Retourne true si le nombre de vie est strictement supérieur à 0</returns>
        public override bool IsAlive()
        {
            return nbVies > 0;
        }
    }
}
